/*
 *  SPDX-License-Identifier: Apache-2.0
 *
 *  Copyright The original authors
 *
 *  Licensed under the Apache Software License version 2.0, available at http://www.apache.org/licenses/LICENSE-2.0
 */
module org.egedede.trip.files {
  requires org.egedede.trip.usecase;
  requires org.egedede.trip.domain;
  requires com.fasterxml.jackson.core;
  requires com.fasterxml.jackson.databind;
  requires com.fasterxml.jackson.datatype.jsr310;
  exports org.egedede.trip.files;
  exports org.egedede.trip.files.domain;

}
