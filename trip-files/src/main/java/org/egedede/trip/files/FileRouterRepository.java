package org.egedede.trip.files;

import org.egedede.trip.usecase.routing.RouterRepository;
import org.egedede.trip.usecase.routing.RoutingRequest;
import org.egedede.trip.usecase.routing.RoutingResponse;

public class FileRouterRepository extends FileRepository implements RouterRepository {
  public FileRouterRepository() {
    super();
  }

  @Override
  public void save(RoutingRequest request, RoutingResponse response) {
    saveRoutings(loadRoutings().put(request, response));
  }

  @Override
  public RoutingResponse route(RoutingRequest request) {
    return loadRoutings().get(request);
  }
}
