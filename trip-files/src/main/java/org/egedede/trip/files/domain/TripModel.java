package org.egedede.trip.files.domain;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import org.egedede.trip.model.Day;
import org.egedede.trip.model.Location;
import org.egedede.trip.model.Trip;
import org.egedede.trip.model.actions.TravelAction;

public record TripModel(List<Location> locations, List<Trip> trips) {

  public static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy_MM_dd");

  public TripModel add(Location newLocation) {
    List<Location> newLocations = locations();
    newLocations.add(newLocation);
    return new TripModel(newLocations, trips());
  }

  public TripModel remove(Location city) {
    List<Location> newLocations = new ArrayList<>(locations().size());
    for (Location location : locations()) {
      if (!Objects.equals(city.id(), location.id())) {
        newLocations.add(location);
      }
    }
    return new TripModel(newLocations, trips);
  }

  public TripModel add(Trip newTrip) {
    List<Trip> newTrips = trips();
    newTrips.add(newTrip);
    return new TripModel(locations(), newTrips);
  }

  public TripModel remove(Trip toDelete) {
    List<Trip> newTrips = new ArrayList<>(trips().size());
    for (Trip trip : trips()) {
      if (!Objects.equals(toDelete.id(), trip.id())) {
        newTrips.add(trip);
      }
    }
    return new TripModel(locations(), newTrips);
  }

  public TripModel removeTripByName(String name) {
    List<Trip> newTrips = new ArrayList<>(trips().size());
    for (Trip trip : trips()) {
      if (!Objects.equals(name, trip.name())) {
        newTrips.add(trip);
      }
    }
    return new TripModel(locations(), newTrips);
  }

  public Trip findTripByName(String name) {
    return trips().stream().filter(t -> Objects.equals(name, t.name())).findFirst().orElse(null);
  }

  public TripModel add(Trip trip, Location location) {
    Trip updatedTrip = findTripByName(trip.name());
    updatedTrip.locations().add(location);
    return this;
  }

  public TripModel remove(Trip trip, Location location) {
    Trip updatedTrip = findTripByName(trip.name());
    updatedTrip.locations().remove(location);
    return this;
  }

  public TripModel resetDays(Trip trip, LocalDate start, LocalDate end) {
    Trip updatedTrip = findTripByName(trip.name());
    updatedTrip.days().clear();
    List<Day> newDays = new ArrayList<>();
    for (LocalDate date = start; date.isBefore(end.plusDays(1)); date = date.plusDays(1)) {
      newDays.add(
          new Day(
              trip.id() + "_" + date.format(DATE_FORMAT),
              date,
              List.of()));
    }
    updatedTrip.days().addAll(newDays);
    return this;
  }

  public TripModel replace(Trip trip, Location toReplace, Location newLocation) {
    Trip updatedTrip = findTripByName(trip.name());

    int indexToReplace = updatedTrip.locations().indexOf(toReplace);
    updatedTrip.locations().set(indexToReplace - 1, newLocation);

    return this;
  }

  public TripModel addTravel(Trip trip, Day toUpdate, TravelAction action) {
    Trip updatedTrip = findTripByName(trip.name());
    List<Day> days = updatedTrip.days();
    int i = days.indexOf(toUpdate);
    days.get(i).actions().add(action);
    return this;
  }
}
