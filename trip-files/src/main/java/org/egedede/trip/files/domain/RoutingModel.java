package org.egedede.trip.files.domain;

import java.util.ArrayList;
import java.util.List;
import org.egedede.trip.usecase.routing.RoutingRequest;
import org.egedede.trip.usecase.routing.RoutingResponse;

public record RoutingModel(List<RoutingRequest> requests, List<RoutingResponse> responses) {

  public RoutingModel put(RoutingRequest request, RoutingResponse response) {
    List<RoutingRequest> newRequests = new ArrayList<>(requests());
    List<RoutingResponse> newResponses = new ArrayList<>(responses());
    int index = newRequests.indexOf(request);
    if(index >= 0) {
      newResponses.set(index, response);
    } else {
      newRequests.add(request);
      newResponses.add(response);
    }
    return new RoutingModel(newRequests, newResponses);

  }

  public RoutingResponse get(RoutingRequest request) {
    int index = requests().indexOf(request);
    return index >= 0 ? responses().get(index) : null;
  }

}
