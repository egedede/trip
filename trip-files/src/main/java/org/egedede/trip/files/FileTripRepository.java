package org.egedede.trip.files;

import java.util.List;

import org.egedede.trip.files.domain.TripModel;
import org.egedede.trip.model.Trip;
import org.egedede.trip.usecase.CreateTripRequest;
import org.egedede.trip.usecase.TripRepository;

public class FileTripRepository extends FileRepository implements TripRepository {

    @Override
    public Trip create(CreateTripRequest request) {
        TripModel tripModel = loadModel();
        Trip trip = new Trip(request.name(), request.name(), request.description(), List.of(), List.of());
        saveModel(tripModel.add(trip));
        return trip;
    }

    @Override
    public void delete(Trip trip) {
        saveModel(loadModel().remove(trip));
    }

    @Override
    public Trip findByName(String name) {
        return loadModel().findTripByName(name);
    }

    @Override
    public List<Trip> list() {
        return loadModel().trips();
    }
}
