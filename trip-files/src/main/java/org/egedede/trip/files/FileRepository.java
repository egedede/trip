package org.egedede.trip.files;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.egedede.trip.files.domain.RoutingModel;
import org.egedede.trip.files.domain.TripModel;

public class FileRepository {
  private static final Path APP_PATH = Path.of(System.getProperty("user.home"), ".etrip");

  static {
    try {
      Files.createDirectories(APP_PATH);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  ObjectMapper mapper = new ObjectMapper();

  public FileRepository() {
    mapper.findAndRegisterModules();
    mapper.registerModule(new JavaTimeModule());
  }

  protected RoutingModel loadRoutings() {
    Path modelFile = APP_PATH.resolve("routing.json");

    try {
      return Files.exists(modelFile)
          ? mapper.readValue(modelFile.toFile(), new TypeReference<>() {})
          : new RoutingModel(List.of(), List.of());
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  protected void saveRoutings(RoutingModel model) {
    Path modelWriteFile = APP_PATH.resolve("routing.json");
    try {
      Files.write(modelWriteFile, mapper.writeValueAsBytes(model));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  protected TripModel loadModel() {
    Path modelFile = APP_PATH.resolve("model.json");

    try {
      return Files.exists(modelFile)
          ? mapper.readValue(modelFile.toFile(), new TypeReference<>() {})
          : new TripModel(new ArrayList<>(0), new ArrayList<>(0));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  protected TripModel saveModel(TripModel tripModel) {
    Path modelWriteFile = APP_PATH.resolve("model.json");
    try {
      Files.write(modelWriteFile, mapper.writerWithDefaultPrettyPrinter().writeValueAsBytes(tripModel));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    return tripModel;
  }
}
