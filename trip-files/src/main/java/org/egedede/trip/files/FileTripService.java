package org.egedede.trip.files;

import java.time.LocalDate;
import org.egedede.trip.files.domain.TripModel;
import org.egedede.trip.model.Day;
import org.egedede.trip.model.Location;
import org.egedede.trip.model.Trip;
import org.egedede.trip.model.actions.TravelAction;
import org.egedede.trip.usecase.TripService;

public class FileTripService extends FileRepository implements TripService {
  @Override
  public Trip add(Trip trip, Location location) {
    TripModel tripModel = loadModel();
    saveModel(tripModel.add(trip, location));
    return trip;
  }

  @Override
  public Trip remove(Trip trip, Location location) {
      TripModel tripModel = loadModel();
      saveModel(tripModel.remove(trip, location));
      return trip;
  }

  @Override
  public Trip resetDays(Trip trip, LocalDate start, LocalDate end) {
    TripModel tripModel = loadModel();
    saveModel(tripModel.resetDays(trip, start, end));
    return trip;
  }

  @Override
  public Trip addTravel(Trip trip, Day day, TravelAction action) {
    TripModel tripModel = loadModel();
    saveModel(tripModel.addTravel(trip, day, action));
    return trip;
  }

  @Override
  public Trip replaceLocation(Trip trip, Location toReplace, Location newLocation) {
    return saveModel(loadModel().replace(trip, toReplace, newLocation)).findTripByName(trip.name());
  }
}
