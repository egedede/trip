package org.egedede.trip.files;

import java.util.List;
import java.util.UUID;

import org.egedede.trip.files.domain.TripModel;
import org.egedede.trip.model.Coordinates;
import org.egedede.trip.model.Location;
import org.egedede.trip.usecase.locate.CityLocation;
import org.egedede.trip.usecase.location.LocationRepository;

public class FileLocationRepository extends FileRepository implements LocationRepository {

    @Override
    public Location add(CityLocation location) {
        TripModel tripModel = loadModel();
        Location newLocation = new Location(
                UUID.randomUUID().toString(),
                location.name(),
                new Coordinates(location.latitude(), location.longitude()),
                location.countryCode());
        saveModel(tripModel.add(newLocation));
        return newLocation;
    }

    @Override
    public List<Location> list() {
        return loadModel().locations();
    }

    @Override
    public void delete(Location city) {
        TripModel tripModel = loadModel();
        saveModel(tripModel.remove(city));
    }
}
