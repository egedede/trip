/*
 *  SPDX-License-Identifier: Apache-2.0
 *
 *  Copyright The original authors
 *
 *  Licensed under the Apache Software License version 2.0, available at http://www.apache.org/licenses/LICENSE-2.0
 */
module org.egedede.trip.locate.openmeteo {
  exports org.egedede.trip.locate.openmeteo;
  requires org.egedede.trip.usecase;
  requires com.google.gson;
  requires org.egedede.trip.domain;
  requires java.net.http;
}
