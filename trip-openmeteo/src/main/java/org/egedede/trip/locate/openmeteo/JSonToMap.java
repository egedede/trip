package org.egedede.trip.locate.openmeteo;

import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class JSonToMap {

    public static Map<String, Object> convert(String json) {
        Gson gson = new Gson();
        TypeToken<Map<String, Object>> mapType = new TypeToken<>() {
        };
        return gson.fromJson(json, mapType);
    }
}
