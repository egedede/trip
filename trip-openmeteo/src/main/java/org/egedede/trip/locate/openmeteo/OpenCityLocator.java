package org.egedede.trip.locate.openmeteo;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.egedede.trip.ApiAnswer;
import org.egedede.trip.exception.TechnicalException;
import org.egedede.trip.usecase.locate.CityLocation;
import org.egedede.trip.usecase.locate.CityLocator;
import org.egedede.trip.usecase.locate.CityLocatorRequest;

public class OpenCityLocator implements CityLocator {

    private static final String COUNT = "count";
    private static final String FORMAT = "format";

    private static final String basePath = "https://geocoding-api.open-meteo.com/v1/search?%s=json&%s=10".formatted(FORMAT, COUNT);
    private static final String NAME = "name";
    private static final String LANGUAGE = "language";
    private final HttpClient client;

    public OpenCityLocator() {
        client = HttpClient.newBuilder().build();
    }

    @Override
    public ApiAnswer<List<CityLocation>> locate(CityLocatorRequest request) throws TechnicalException {
        HttpRequest httpRequest = HttpRequest.newBuilder().uri(URI.create("%s%s%s".formatted(
                basePath,
                request.cityName().isBlank() ? "" : "&" + NAME + "=" + request.cityName(),
                request.locale().isBlank() ? "" : "&" + LANGUAGE + "=" + request.locale()))).build();
        HttpResponse<String> response = null;
        try {
            response = client.send(httpRequest, HttpResponse.BodyHandlers.ofString());
        }
        catch (IOException | InterruptedException e) {
            throw new TechnicalException("Failed to request location", e);
        }
        if (response.statusCode() < 400) { // success
            Map<String, Object> resultMap = JSonToMap.convert(response.body());
            List<CityLocation> subResults = new ArrayList<>(10);
            List<Map<String, Object>> subMap = (List<Map<String, Object>>) resultMap.get("results");
            for (Map<String, Object> values : subMap) {
                subResults.add(new CityLocation(
                        (String) values.get("name"),
                        (double) values.get("latitude"),
                        (double) values.get("longitude"),
                        (String) values.get("country_code"),
                        (String) values.get("timezone")));
            }

            return ApiAnswer.success(subResults, response.statusCode());
        }
        else {
            return ApiAnswer.failure(response.statusCode(), response.body());
        }
    }

}
