package org.egedede.trip.locate.openmeteo;

public record GeocodingResult(String name, double latitude, double longitude, String countryCode, String timeZone) {

}
