/*
 *  SPDX-License-Identifier: Apache-2.0
 *
 *  Copyright The original authors
 *
 *  Licensed under the Apache Software License version 2.0, available at http://www.apache.org/licenses/LICENSE-2.0
 */
module org.egedede.trip.openrouteservice {
  exports org.egedede.trip.routing.openrouteservice;

  requires org.egedede.trip.usecase;
  requires org.egedede.trip.domain;
  requires java.net.http;
  requires com.fasterxml.jackson.databind;
}
