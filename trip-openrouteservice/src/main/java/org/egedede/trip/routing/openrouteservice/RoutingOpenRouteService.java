package org.egedede.trip.routing.openrouteservice;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.List;
import java.util.Map;

import org.egedede.trip.ApiAnswer;
import org.egedede.trip.model.Distance;
import org.egedede.trip.model.DistanceUnit;
import org.egedede.trip.model.RoutingMode;
import org.egedede.trip.usecase.routing.Router;
import org.egedede.trip.usecase.routing.RoutingRequest;
import org.egedede.trip.usecase.routing.RoutingResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class RoutingOpenRouteService implements Router {
    private static final String basePathFormat = "https://api.openrouteservice.org/v2/directions/%s/json";
    private final HttpClient client;

    public RoutingOpenRouteService() {
        client = HttpClient.newBuilder().build();
    }

    public ApiAnswer<RoutingResponse> route(RoutingRequest request) {
        String requestBody = """
                {"coordinates":[[%s,%s],[%s,%s]],
                "maximum_speed":%s}
                """.formatted(
                request.start().coordinates().longitude(), request.start().coordinates().latitude(),
                request.end().coordinates().longitude(), request.end().coordinates().latitude(),
                request.maxSpeed());
        System.out.println("Request body");
        String uri = basePathFormat.formatted(getProfile(request.mode()).getLabel());
        System.out.println(uri);
        HttpRequest httpRequest = HttpRequest.newBuilder().uri(URI.create(
                uri))
                .header("Authorization", System.getenv("OPENROUTESERVICE_TOKEN"))
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(
                        requestBody))
                .build();
        HttpResponse<String> response;
        try {
            response = client.send(httpRequest, HttpResponse.BodyHandlers.ofString());
        }
        catch (IOException | InterruptedException e) {
            return ApiAnswer.failure(500, e.getMessage());
        }
        if (response.statusCode() < 400) { //
            ObjectMapper mapper = new ObjectMapper();
            Map<String, Object> resultMap = null;
            try {
                resultMap = mapper.readValue(response.body(), new TypeReference<>() {
                });
            }
            catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
            Map<String, Object> routeMap = ((List<Map<String, Object>>) resultMap.get("routes")).get(0);
            Map<String, Object> summary = (Map<String, Object>) routeMap.get("summary");
            Distance distance = new Distance((Double) summary.get("distance"), DistanceUnit.METER);
            Duration duration = Duration.ofSeconds(((Double) summary.get("duration")).longValue());
            return ApiAnswer.success(new RoutingResponse(request.mode(), request.start(), request.end(), distance, duration));
        }
        else {
            return ApiAnswer.failure(response.statusCode(), response.body());
        }
    }

    private RoutingProfile getProfile(RoutingMode mode) {
        return switch (mode) {
            case CAR -> RoutingProfile.driving_car;
            case FEET -> RoutingProfile.foot_walking;
        };
    }
}
