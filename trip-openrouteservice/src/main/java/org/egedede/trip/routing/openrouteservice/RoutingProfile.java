package org.egedede.trip.routing.openrouteservice;

public enum RoutingProfile {
    driving_car("driving-car"),
    driving_hgv("driving-hgv"),
    cycling_regular("cycling-regular"),
    cycling_road("cycling-road"),
    cycling_mountain("cycling-mountain"),
    cycling_electric("cycling-electric"),
    foot_walking("foot-walking"),
    foot_hiking("foot-hiking"),
    wheelchair("wheelchair");

    private final String label;

    RoutingProfile(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
