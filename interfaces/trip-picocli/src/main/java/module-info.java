/*
 *  SPDX-License-Identifier: Apache-2.0
 *
 *  Copyright The original authors
 *
 *  Licensed under the Apache Software License version 2.0, available at http://www.apache.org/licenses/LICENSE-2.0
 */
module org.egedede.trip.cli {
  requires info.picocli;
  requires org.egedede.trip.usecase;
  requires org.egedede.trip.locate.openmeteo;
  requires org.egedede.trip.domain;
  requires com.fasterxml.jackson.core;
  requires com.fasterxml.jackson.databind;
  requires org.egedede.trip.files;
  requires org.egedede.trip.openrouteservice;
  opens org.egedede.trip.cli;
  opens org.egedede.trip.cli.commands;
  opens org.egedede.trip.cli.commands.city;
  opens org.egedede.trip.cli.commands.trip;
}
