package org.egedede.trip.cli.repository;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.egedede.trip.usecase.locate.CityLocation;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class WorkingRepository {

    private static final Path APP_PATH = Path.of(System.getProperty("user.home"), ".etrip");

    static {
        try {
            Files.createDirectories(APP_PATH);
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    ObjectMapper mapper = new ObjectMapper();

    public void storeCities(List<CityLocation> locations) {
        try {
            Files.write(APP_PATH.resolve("working_cities.json"), mapper.writeValueAsBytes(locations));
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public List<CityLocation> getCities() {
        try {
            return mapper.readValue(APP_PATH.resolve("working_cities.json").toFile(), new TypeReference<>() {
            });
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
