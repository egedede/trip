package org.egedede.trip.cli.commands.trip;

import static org.egedede.trip.cli.CliHelpers.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.Callable;
import org.egedede.trip.files.FileLocationRepository;
import org.egedede.trip.files.FileTripRepository;
import org.egedede.trip.files.FileTripService;
import org.egedede.trip.model.Trip;
import org.egedede.trip.usecase.TripRepository;
import org.egedede.trip.usecase.TripService;
import org.egedede.trip.usecase.location.LocationRepository;
import picocli.CommandLine;

@CommandLine.Command(name = "reset-days")
public class TripResetDaysCli implements Callable<Integer> {

  private final TripRepository tripRepository = new FileTripRepository();
  private final TripService tripService = new FileTripService();
  private final LocationRepository locationRepository = new FileLocationRepository();

  @Override
  public Integer call() throws Exception {
    List<Trip> trips = tripRepository.list();
    Trip trip;
    if (trips.size() == 1) {
      trip = trips.get(0);
    } else {
      listTrips(trips);
      int tripIn = readInt("Choose a trip : ", 1);
      trip = trips.get(tripIn - 1);
    }
    String startDate = readString("Input start date (yyyy-MM-dd) : ");
    String endDate = readString("Input end date (yyyy-MM-dd) : ");
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    tripService.resetDays(
        trip, LocalDate.parse(startDate, formatter), LocalDate.parse(endDate, formatter));
    return 0;
  }
}
