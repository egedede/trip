package org.egedede.trip.cli.commands.trip;

import static org.egedede.trip.cli.CliHelpers.*;

import java.util.List;
import java.util.concurrent.Callable;
import org.egedede.trip.files.FileLocationRepository;
import org.egedede.trip.files.FileTripRepository;
import org.egedede.trip.files.FileTripService;
import org.egedede.trip.model.Location;
import org.egedede.trip.model.Trip;
import org.egedede.trip.usecase.TripRepository;
import org.egedede.trip.usecase.TripService;
import org.egedede.trip.usecase.location.LocationRepository;
import picocli.CommandLine;

@CommandLine.Command(name = "replace-city")
public class TripReplaceCityCli implements Callable<Integer> {

  private final TripRepository tripRepository = new FileTripRepository();
  private final TripService tripService = new FileTripService();
  private final LocationRepository locationRepository = new FileLocationRepository();

  @Override
  public Integer call() throws Exception {
    List<Trip> trips = tripRepository.list();
    Trip trip;
    if(trips.size()==1) {
      trip = trips.get(0);
      System.out.println("Selected trip is "+trip.name());
    } else {
      listTrips(trips);
      int tripIn = readInt("Choose a trip : ", 1);
      trip = trips.get(tripIn - 1);
    }
    listLocations(trip.locations());
    int locationToReplace = readInt("Choose a location to replace : ");
    List<Location> locations = locationRepository.list();
    listLocations(locations);
    int locationIn = readInt("Choose the new location : ");
    Location location = locations.get(locationIn - 1);
    tripService.replaceLocation(trip, trip.locations().get(locationToReplace), location);
    return 0;
  }
}
