package org.egedede.trip.cli.commands.city;

import java.util.List;
import java.util.concurrent.Callable;

import org.egedede.trip.ApiAnswer;
import org.egedede.trip.cli.repository.WorkingRepository;
import org.egedede.trip.locate.openmeteo.OpenCityLocator;
import org.egedede.trip.usecase.locate.CityLocation;
import org.egedede.trip.usecase.locate.CityLocator;
import org.egedede.trip.usecase.locate.CityLocatorRequest;

import com.fasterxml.jackson.databind.ObjectMapper;

import picocli.CommandLine;

/** Search for a city with given parameters Returns a list of cities. */
@CommandLine.Command(name = "find")
public class CityLocatorCli implements Callable<Integer> {

    private static final ObjectMapper MAPPER = new ObjectMapper();
    @CommandLine.Option(names = { "-city" })
    private String cityName;

    @CommandLine.Option(names = { "-locale" })
    private String locale = "fr";

    @Override
    public Integer call() throws Exception {
        CityLocator locator = new OpenCityLocator();
        CityLocatorRequest request = new CityLocatorRequest(cityName, locale);
        ApiAnswer<List<CityLocation>> response = locator.locate(request);
        if (response.message() == null || !response.message().isBlank()) {
            int i = 1;
            for (CityLocation location : response.response()) {
                System.out.printf(
                        "[%s] %s (%s) : (%s,%s) - %s%n",
                        i++,
                        location.name(),
                        location.countryCode(),
                        location.latitude(),
                        location.longitude(),
                        location.timeZone());
            }
            new WorkingRepository().storeCities(response.response());
            return 0;
        }
        System.err.printf("[%s] %s", response.code(), response.message());
        return -1;
    }
}
