package org.egedede.trip.cli.commands;

import org.egedede.trip.cli.commands.city.*;

import picocli.CommandLine;

/** City cli realm */
@CommandLine.Command(name = "city", subcommands = { CityLocatorCli.class, CityLocateAndSaveCli.class, CitySaveCli.class, CityListCli.class, CityDistanceCli.class })
public class CityCli {
}
