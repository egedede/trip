package org.egedede.trip.cli.commands.trip;

import java.util.concurrent.Callable;
import org.egedede.trip.files.FileTripRepository;
import org.egedede.trip.usecase.TripRepository;
import picocli.CommandLine;

@CommandLine.Command(name = "delete")
public class TripDeleteCli implements Callable<Integer> {

  private final TripRepository tripRepository = new FileTripRepository();

  @CommandLine.Option(names = {"-index"})
  private int index;

  @Override
  public Integer call() throws Exception {
    tripRepository.delete(tripRepository.list().get(index - 1));
    return 0;
  }
}
