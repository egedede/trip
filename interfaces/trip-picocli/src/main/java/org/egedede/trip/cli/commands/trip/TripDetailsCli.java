package org.egedede.trip.cli.commands.trip;

import static org.egedede.trip.cli.CliHelpers.*;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;
import org.egedede.trip.ApiAnswer;
import org.egedede.trip.files.FileRouterRepository;
import org.egedede.trip.files.FileTripRepository;
import org.egedede.trip.model.DistanceUnit;
import org.egedede.trip.model.Location;
import org.egedede.trip.model.RoutingMode;
import org.egedede.trip.model.Trip;
import org.egedede.trip.routing.openrouteservice.RoutingOpenRouteService;
import org.egedede.trip.usecase.TripRepository;
import org.egedede.trip.usecase.routing.Router;
import org.egedede.trip.usecase.routing.RouterBackedByRepository;
import org.egedede.trip.usecase.routing.RoutingRequest;
import org.egedede.trip.usecase.routing.RoutingResponse;
import picocli.CommandLine;

@CommandLine.Command(name = "details")
public class TripDetailsCli implements Callable<Integer> {

  private final TripRepository tripRepository = new FileTripRepository();
  private final Router router =
      new RouterBackedByRepository(new RoutingOpenRouteService(), new FileRouterRepository());

  @Override
  public Integer call() throws Exception {
    List<Trip> trips = tripRepository.list();
    Trip trip;
    if (trips.size() == 1) {
      trip = trips.get(0);
    } else {
      listTrips(trips);
      int tripIn = readInt("Choose a trip : ", 1);
      trip = trips.get(tripIn - 1);
    }
    System.out.printf(
        """
        Name : %s,
        Description : %s
        Cities :
        %s
        Days : %s
        """,
        trip.name(),
        trip.description(),
        displayLocations(trip.locations()),
        trip.days().stream().map(d -> " * [%s] : %s".formatted(d.toHumanString(),
            d.actionsDetails())).collect(Collectors.joining("\n")));
    return 0;
  }

  private String displayLocations(List<Location> locations) {
    StringBuilder result = new StringBuilder();
    Location previous = null;
    for (Location location : locations) {
      if (previous != null && !Objects.equals(previous,location)) {
        ApiAnswer<RoutingResponse> route =
            router.route(new RoutingRequest(RoutingMode.CAR, previous, location, 110));
        if (route.isSuccess()) {
          RoutingResponse response = route.response();
          result.append(" -> %s (%s)%n".formatted(response.distance().to(DistanceUnit.KILOMETER), response.duration()));
        } else {
          result.append("Failed to get distance\n");
        }
      }
      result.append(" * ").append(location.toHumanString()).append("\n");
      previous = location;
    }
    ApiAnswer<RoutingResponse> route =
        router.route(new RoutingRequest(RoutingMode.CAR, previous, locations.get(0), 110));
    if (route.isSuccess()) {
      RoutingResponse response = route.response();
      result.append(" -> %s (%s)%n".formatted(response.distance().to(DistanceUnit.KILOMETER), response.duration()));
    } else {
      result.append("Failed to get distance\n");
    }
    result.append(" * ").append(locations.get(0).toHumanString()).append("\n");

    return result.toString();
  }
}
