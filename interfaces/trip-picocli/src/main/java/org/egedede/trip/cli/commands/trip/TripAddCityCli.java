package org.egedede.trip.cli.commands.trip;

import static org.egedede.trip.cli.CliHelpers.*;

import java.util.List;
import java.util.concurrent.Callable;
import org.egedede.trip.files.FileLocationRepository;
import org.egedede.trip.files.FileTripRepository;
import org.egedede.trip.files.FileTripService;
import org.egedede.trip.model.Location;
import org.egedede.trip.model.Trip;
import org.egedede.trip.usecase.TripRepository;
import org.egedede.trip.usecase.TripService;
import org.egedede.trip.usecase.location.LocationRepository;
import picocli.CommandLine;

@CommandLine.Command(name = "add-city")
public class TripAddCityCli implements Callable<Integer> {

  private final TripRepository tripRepository = new FileTripRepository();
  private final TripService tripService = new FileTripService();
  private final LocationRepository locationRepository = new FileLocationRepository();

  @Override
  public Integer call() throws Exception {
    List<Trip> trips = tripRepository.list();
    listTrips(trips);
    int tripIn = readInt("Choose a trip : ", 1);
    Trip trip = trips.get(tripIn - 1);
    List<Location> locations = locationRepository.list();
    listLocations(locations);
    int locationIn = readInt("Choose a location to add : ");
    Location location = locations.get(locationIn - 1);
    tripService.add(trip, location);
    return 0;
  }
}
