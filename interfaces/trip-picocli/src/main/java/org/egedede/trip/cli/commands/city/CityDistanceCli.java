package org.egedede.trip.cli.commands.city;

import java.util.List;
import java.util.concurrent.Callable;

import org.egedede.trip.ApiAnswer;
import org.egedede.trip.files.FileLocationRepository;
import org.egedede.trip.model.Location;
import org.egedede.trip.model.RoutingMode;
import org.egedede.trip.routing.openrouteservice.RoutingOpenRouteService;
import org.egedede.trip.usecase.location.LocationRepository;
import org.egedede.trip.usecase.routing.Router;
import org.egedede.trip.usecase.routing.RoutingRequest;
import org.egedede.trip.usecase.routing.RoutingResponse;

import picocli.CommandLine;

/** Search for a city with given parameters Returns a list of cities. */
@CommandLine.Command(name = "distance")
public class CityDistanceCli implements Callable<Integer> {

    private LocationRepository locationRepository = new FileLocationRepository();
    private Router router = new RoutingOpenRouteService();

    @CommandLine.Option(names = { "-start" })
    private int start;
    @CommandLine.Option(names = { "-end" })
    private int end;

    @Override
    public Integer call() {
        List<Location> locations = locationRepository.list();
        ApiAnswer<RoutingResponse> response = router.route(new RoutingRequest(RoutingMode.CAR, locations.get(start - 1), locations.get(end - 1), 110));
        if (response.message() == null || !response.message().isBlank()) {
            RoutingResponse routing = response.response();
            System.out.printf("%s , %s", routing.distance(), routing.duration());
            return 0;
        }
        System.err.printf("[%s] %s", response.code(), response.message());
        return -1;
    }
}
