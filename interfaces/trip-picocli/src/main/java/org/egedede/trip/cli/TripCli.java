package org.egedede.trip.cli;

import org.egedede.trip.cli.commands.CityCli;
import org.egedede.trip.cli.commands.trip.*;
import picocli.CommandLine;
import picocli.CommandLine.Command;

@Command(
    name = "trip",
    subcommands = {
      CityCli.class,
      TripNewCli.class,
      TripListCli.class,
      TripDeleteCli.class,
      TripAddCityCli.class,
      TripDetailsCli.class,
      TripRemoveCityCli.class,
      TripReplaceCityCli.class,
      TripResetDaysCli.class,
      TripAddCityToDayCli.class
    })
public class TripCli {
  public static void main(String[] args) {
    int exitCode = new CommandLine(new TripCli()).execute(args);
    System.exit(exitCode);
  }
}
