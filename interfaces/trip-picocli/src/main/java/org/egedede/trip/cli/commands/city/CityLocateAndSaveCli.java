package org.egedede.trip.cli.commands.city;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;
import java.util.concurrent.Callable;
import org.egedede.trip.ApiAnswer;
import org.egedede.trip.cli.repository.WorkingRepository;
import org.egedede.trip.files.FileLocationRepository;
import org.egedede.trip.locate.openmeteo.OpenCityLocator;
import org.egedede.trip.usecase.locate.CityLocation;
import org.egedede.trip.usecase.locate.CityLocator;
import org.egedede.trip.usecase.locate.CityLocatorRequest;
import org.egedede.trip.usecase.location.LocationRepository;
import picocli.CommandLine;

import static org.egedede.trip.cli.CliHelpers.readInt;
import static org.egedede.trip.cli.CliHelpers.readString;

/** Search for a city with given parameters Returns a list of cities. */
@CommandLine.Command(name = "locate")
public class CityLocateAndSaveCli implements Callable<Integer> {

  private LocationRepository locationRepository = new FileLocationRepository();

  @Override
  public Integer call() throws Exception {
    CityLocator locator = new OpenCityLocator();
    String cityName = readString("City name : ");

    CityLocatorRequest request = new CityLocatorRequest(cityName, "fr");
    ApiAnswer<List<CityLocation>> response = locator.locate(request);
    if (response.message() == null || !response.message().isBlank()) {
      int i = 1;
      List<CityLocation> cities = response.response();
      for (CityLocation location : cities) {
        System.out.printf(
            "[%s] %s (%s) : (%s,%s) - %s%n",
            i++,
            location.name(),
            location.countryCode(),
            location.latitude(),
            location.longitude(),
            location.timeZone());
      }
      int chosenCity = readInt("Select the city to add : ", 1);
      if (chosenCity < 1 || chosenCity > cities.size()) {
        System.err.println("Please select a valid index");
        return -1;
      }
      CityLocation location = cities.get(chosenCity - 1);
      System.out.printf(
          "Chosen city is : %s (%s) : (%s,%s) - %s%n",
          location.name(),
          location.countryCode(),
          location.latitude(),
          location.longitude(),
          location.timeZone());
      locationRepository.add(location);

      return 0;
    } else {
      System.err.printf("[%s] %s", response.code(), response.message());
      return -1;
    }
  }
}
