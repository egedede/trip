package org.egedede.trip.cli.commands.city;

import java.util.List;
import java.util.concurrent.Callable;

import org.egedede.trip.files.FileLocationRepository;
import org.egedede.trip.model.Location;
import org.egedede.trip.usecase.location.LocationRepository;

import picocli.CommandLine;

/** Search for a city with given parameters Returns a list of cities. */
@CommandLine.Command(name = "list")
public class CityListCli implements Callable<Integer> {

    private LocationRepository locationRepository = new FileLocationRepository();

    @Override
    public Integer call() {
        List<Location> locations = locationRepository.list();
        int i = 1;
        for (Location location : locations) {
            System.out.printf(
                    "[%s] %s (%s) : (%s,%s)%n",
                    i++,
                    location.name(),
                    location.country(),
                    location.coordinates().latitude(),
                    location.coordinates().longitude());
        }
        return 0;
    }
}
