package org.egedede.trip.cli.commands.city;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;
import java.util.concurrent.Callable;

import org.egedede.trip.cli.repository.WorkingRepository;
import org.egedede.trip.files.FileLocationRepository;
import org.egedede.trip.usecase.locate.CityLocation;
import org.egedede.trip.usecase.location.LocationRepository;

import picocli.CommandLine;

/** Search for a city with given parameters Returns a list of cities. */
@CommandLine.Command(name = "save")
public class CitySaveCli implements Callable<Integer> {

    private LocationRepository locationRepository = new FileLocationRepository();

    @Override
    public Integer call() throws Exception {
        List<CityLocation> cities = new WorkingRepository().getCities();
        int i = 1;
        for (CityLocation location : cities) {
            System.out.printf(
                    "[%s] %s (%s) : (%s,%s) - %s%n",
                    i++,
                    location.name(),
                    location.countryCode(),
                    location.latitude(),
                    location.longitude(),
                    location.timeZone());
        }
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Choose a city to save :");
        String chosenCity = br.readLine();
        if (Integer.parseInt(chosenCity) < 1 || Integer.parseInt(chosenCity) > cities.size()) {
            System.err.println("Please select a valid index");
            return -1;
        }
        CityLocation location = cities.get(Integer.parseInt(chosenCity) - 1);
        System.out.printf("Chosen city is : %s (%s) : (%s,%s) - %s%n",
                location.name(),
                location.countryCode(),
                location.latitude(),
                location.longitude(),
                location.timeZone());
        locationRepository.add(location);
        return 0;
    }
}
