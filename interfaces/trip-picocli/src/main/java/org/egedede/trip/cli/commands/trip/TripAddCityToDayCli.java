package org.egedede.trip.cli.commands.trip;

import static org.egedede.trip.cli.CliHelpers.*;

import java.util.List;
import java.util.concurrent.Callable;
import org.egedede.trip.ApiAnswer;
import org.egedede.trip.files.FileLocationRepository;
import org.egedede.trip.files.FileRouterRepository;
import org.egedede.trip.files.FileTripRepository;
import org.egedede.trip.files.FileTripService;
import org.egedede.trip.model.*;
import org.egedede.trip.model.actions.TravelAction;
import org.egedede.trip.routing.openrouteservice.RoutingOpenRouteService;
import org.egedede.trip.usecase.TripRepository;
import org.egedede.trip.usecase.TripService;
import org.egedede.trip.usecase.location.LocationRepository;
import org.egedede.trip.usecase.routing.Router;
import org.egedede.trip.usecase.routing.RouterBackedByRepository;
import org.egedede.trip.usecase.routing.RoutingRequest;
import org.egedede.trip.usecase.routing.RoutingResponse;
import picocli.CommandLine;

@CommandLine.Command(name = "add-city-to-day")
public class TripAddCityToDayCli implements Callable<Integer> {

  private final TripRepository tripRepository = new FileTripRepository();
  private final TripService tripService = new FileTripService();
  private final LocationRepository locationRepository = new FileLocationRepository();

  @Override
  public Integer call() throws Exception {
    List<Trip> trips = tripRepository.list();
    Trip trip;
    if (trips.size() == 1) {
      trip = trips.get(0);
    } else {
      listTrips(trips);
      int tripIn = readInt("Choose a trip : ", 1);
      trip = trips.get(tripIn - 1);
    }
    List<Location> locations = trip.locations();
    listLocations(locations);
    int locationIn = readInt("Choose a location to add : ");
    Location location = locations.get(locationIn - 1);
    List<Day> days = trip.days();
    listDays(days);
    int daysIn = readInt("Choose the date to add to : ");
    Day day = days.get(daysIn - 1);

    Router router =
        new RouterBackedByRepository(new RoutingOpenRouteService(), new FileRouterRepository());
    Location start = getStart(trip, day);
    ApiAnswer<RoutingResponse> response =
        router.route(new RoutingRequest(RoutingMode.CAR, start, location, 110));
    if (response.isSuccess()) {
      Route route =
          new Route(
              start,
              location,
              response.response().routingMode(),
              110,
              response.response().duration(),
              response.response().distance());
      TravelAction action = new TravelAction(route);
      tripService.addTravel(trip, day, action);
      return 0;
    } else {
      System.err.println("Failed to route from %s to %s".formatted(start, location));
      return -1;
    }
  }

  // iterate over actions to check previous TravelAction and its destination. If none, first city in
  // locations
  private Location getStart(Trip trip, Day targetDay) {
    Location result = null;
    for (Day day : trip.days()) {
      for(Action action : day.actions()) {
        if(action instanceof TravelAction a) {
          result = a.route().end();
        }
      }
      if(day.toHumanString().equals(targetDay.toHumanString())) break;
    }

    if (result == null) result = trip.locations().get(0);
    return result;
  }
}
