package org.egedede.trip.cli;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

import org.egedede.trip.model.Day;
import org.egedede.trip.model.Location;
import org.egedede.trip.model.Trip;

public class CliHelpers {

  public static void listTrips(List<Trip> trips) {
    StringBuilder builder = new StringBuilder();
    int i = 1;
    for (Trip trip : trips) {
      builder.append("[%s] %s : %s%n".formatted(i++, trip.name(), trip.description()));
    }
    System.out.println(builder);
  }

  public static void listDays(List<Day> days) {
    StringBuilder builder = new StringBuilder();
    int i = 1;
    for (Day day : days) {
      builder.append(
          "[%s] %s : %s%n"
              .formatted(
                  i++,
                  day.date(),
                  day.actionsDetails()));
    }
    System.out.println(builder);
  }

  public static void listLocations(List<Location> locations) {
    StringBuilder builder = new StringBuilder();
    int i = 1;
    for (Location location : locations) {
      builder.append(
          "[%s] %s (%s) : (%s,%s)%n"
              .formatted(
                  i++,
                  location.name(),
                  location.country(),
                  location.coordinates().latitude(),
                  location.coordinates().longitude()));
    }
    System.out.println(builder);
  }

  public static int readInt(String prompt) throws Exception {
    String chosenIndex = readString(prompt);
    return Integer.parseInt(chosenIndex);
  }
  public static int readInt(String prompt, int defaultValue) throws Exception {
    String chosenIndex = readString(prompt);
    if(chosenIndex.isBlank()) return defaultValue;
    return Integer.parseInt(chosenIndex);
  }
  public static String readString(String prompt) throws Exception {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    System.out.print(prompt);
    return br.readLine();
  }
}
