package org.egedede.trip.cli.commands.trip;

import java.util.List;
import java.util.concurrent.Callable;
import org.egedede.trip.files.FileTripRepository;
import org.egedede.trip.model.Trip;
import org.egedede.trip.usecase.TripRepository;
import picocli.CommandLine;

@CommandLine.Command(name = "list")
public class TripListCli implements Callable<Integer> {

  private final TripRepository tripRepository = new FileTripRepository();

  @Override
  public Integer call() {
    List<Trip> list = tripRepository.list();
    int i = 1;
    for (Trip trip : list) {
      System.out.printf("[%s] %s : %s%n", i++, trip.name(), trip.description());
    }
    return 0;
  }
}
