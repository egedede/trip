package org.egedede.trip.cli.commands.trip;

import java.util.concurrent.Callable;
import org.egedede.trip.files.FileTripRepository;
import org.egedede.trip.model.Trip;
import org.egedede.trip.usecase.CreateTripRequest;
import org.egedede.trip.usecase.TripRepository;
import picocli.CommandLine;

@CommandLine.Command(name = "new")
public class TripNewCli implements Callable<Integer> {

    private final TripRepository tripRepository = new FileTripRepository();

    @CommandLine.Option(names = { "-name" })
    private String name;

    @CommandLine.Option(names = { "-description" })
    private String description;

    @Override
    public Integer call() throws Exception {
        Trip byName = tripRepository.findByName(name);
        if (byName != null) {
            System.err.println("Trip " + name + " already exists.");
            System.exit(-1);
        }
        tripRepository.create(new CreateTripRequest(name, description));
        return 0;
    }
}
