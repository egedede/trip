package org.egedede.trip;

public record ApiAnswer<T>(T response, int code, String message) {

  public static <Y> ApiAnswer<Y> success(Y response) {
    return new ApiAnswer<>(response, 200, null);
  }

  public static <Y> ApiAnswer<Y> success(Y response, int code) {
    return new ApiAnswer<>(response, code, null);
  }

  public static <Y> ApiAnswer<Y> failure(int code, String message) {
    return new ApiAnswer<>(null, code, message);
  }

  public boolean isSuccess() {
    return code < 400;
  }
}
