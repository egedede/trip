package org.egedede.trip.model;

public record Location(String id, String name, Coordinates coordinates, String country) {
  public String toHumanString() {
    return "%s (%s) : (%s,%s)"
        .formatted(name(), country(), coordinates().latitude(), coordinates().longitude());
  }
}
