package org.egedede.trip.model;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public record Distance(Double value, DistanceUnit unit) {

  public String toString() {
    return value + " " + unit;
  }

  public Distance to(DistanceUnit distanceUnit) {
    return new Distance(
        new BigDecimal(
                value() * unit.getFactorToConvention() / distanceUnit.getFactorToConvention(),
                new MathContext(10, RoundingMode.HALF_EVEN))
            .setScale(0, RoundingMode.HALF_EVEN)
            .doubleValue(),
        distanceUnit);
  }
}
