package org.egedede.trip.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Describes what happens during this day
 */
public record Day(String id, LocalDate date, List<Action> actions) {
  public String toHumanString() {
    return date().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
  }

  public String actionsDetails() {
    return actions().stream().map(action -> action.details()).collect(Collectors.joining(","));
  }
}
