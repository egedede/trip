package org.egedede.trip.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.egedede.trip.model.actions.TravelAction;

@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.PROPERTY,
    property = "type")
@JsonSubTypes({
    @JsonSubTypes.Type(value = TravelAction.class, name = "TRAVEL")
})
public interface Action {
    Type type();

  String details();

  enum Type {
        TRAVEL,
        VISIT,
        REST,

    }
}
