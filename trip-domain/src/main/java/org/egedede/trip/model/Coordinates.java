package org.egedede.trip.model;

public record Coordinates(double latitude, double longitude) {
}
