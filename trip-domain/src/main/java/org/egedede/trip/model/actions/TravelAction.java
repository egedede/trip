package org.egedede.trip.model.actions;

import org.egedede.trip.model.Action;
import org.egedede.trip.model.DistanceUnit;
import org.egedede.trip.model.Route;

public record TravelAction(Route route) implements Action {
  @Override
  public Type type() {
    return Type.TRAVEL;
  }

  @Override
  public String details() {
    return "%s --> %s : %s , %s".formatted(
        route().start().name(),
        route().end().name(),
        route().distance().to(DistanceUnit.KILOMETER),
        route().duration()
    );
  }
}
