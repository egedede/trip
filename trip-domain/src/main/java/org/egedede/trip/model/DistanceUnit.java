package org.egedede.trip.model;

public enum DistanceUnit {
  MILLIMETER(0.001f),
  CENTIMETER(0.01f),
  DECIMETER(0.1f),
  METER(1f),
  DECAMETER(10f),
  KILOMETER(1_000f),
  ;

  private final float factorToConvention;

  DistanceUnit(float factorToConvention) {
    this.factorToConvention = factorToConvention;
  }

  public float getFactorToConvention() {
    return factorToConvention;
  }
}
