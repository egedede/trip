package org.egedede.trip.model;

import java.time.Duration;

public record Route(
    Location start, Location end, RoutingMode mode, int maxSpeed, Duration duration, Distance distance) {}
