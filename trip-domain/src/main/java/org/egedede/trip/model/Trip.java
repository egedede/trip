package org.egedede.trip.model;

import java.util.List;

/** A trip is a list of DayTrip with comments */
public record Trip(
    String id, String name, String description, List<Location> locations, List<Day> days) {}
