package org.egedede.trip.exception;

/**
 * Exception not related to functionality but to outside exceptions.
 *
 * Could be related to a 500 in HTTTP.
 */
public class TechnicalException extends RuntimeException {
    public TechnicalException(String message, Throwable cause) {
        super(message, cause);
    }
}
