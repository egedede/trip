/*
 *  SPDX-License-Identifier: Apache-2.0
 *
 *  Copyright The original authors
 *
 *  Licensed under the Apache Software License version 2.0, available at http://www.apache.org/licenses/LICENSE-2.0
 */
module org.egedede.trip.domain {
  requires com.fasterxml.jackson.annotation;
  exports org.egedede.trip;
  exports org.egedede.trip.model;
  exports org.egedede.trip.exception;
  exports org.egedede.trip.model.actions;
}
