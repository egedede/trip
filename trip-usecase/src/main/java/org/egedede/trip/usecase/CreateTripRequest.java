package org.egedede.trip.usecase;

public record CreateTripRequest (String name, String description){}
