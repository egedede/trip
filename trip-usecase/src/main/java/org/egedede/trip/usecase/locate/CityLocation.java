package org.egedede.trip.usecase.locate;

public record CityLocation(String name, double latitude, double longitude, String countryCode, String timeZone) {}
