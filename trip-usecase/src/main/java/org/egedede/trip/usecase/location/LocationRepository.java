package org.egedede.trip.usecase.location;

import java.util.List;

import org.egedede.trip.model.Location;
import org.egedede.trip.usecase.locate.CityLocation;

public interface LocationRepository {
    public Location add(CityLocation location);

    public List<Location> list();

    public void delete(Location city);
}
