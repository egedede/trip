package org.egedede.trip.usecase.locate;

public record CityLocatorRequest(String cityName, String locale) {

}
