package org.egedede.trip.usecase.routing;

import org.egedede.trip.model.Location;
import org.egedede.trip.model.RoutingMode;

public record RoutingRequest(RoutingMode mode, Location start, Location end, int maxSpeed) {
}
