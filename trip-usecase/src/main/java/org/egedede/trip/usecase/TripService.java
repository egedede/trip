package org.egedede.trip.usecase;

import org.egedede.trip.model.Day;
import org.egedede.trip.model.Location;
import org.egedede.trip.model.Trip;
import org.egedede.trip.model.actions.TravelAction;

import java.time.LocalDate;

public interface TripService {

  Trip add(Trip trip, Location location);
  Trip remove(Trip trip, Location location);

  Trip resetDays(Trip trip, LocalDate start, LocalDate end);

  Trip addTravel(Trip trip, Day day, TravelAction action);

  Trip replaceLocation(Trip trip, Location toReplace, Location newLocation);
}
