package org.egedede.trip.usecase;

import java.util.List;
import org.egedede.trip.model.Trip;

public interface TripRepository {

  Trip create(CreateTripRequest request);

  void delete(Trip trip);

  List<Trip> list();

  Trip findByName(String name);
}
