package org.egedede.trip.usecase.routing;

import org.egedede.trip.ApiAnswer;

public interface Router {
    ApiAnswer<RoutingResponse> route(RoutingRequest request);
}
