package org.egedede.trip.usecase.routing;

import org.egedede.trip.ApiAnswer;

public class RouterBackedByRepository implements Router {

  private Router router;
  private RouterRepository repository;

  public RouterBackedByRepository(Router router, RouterRepository repository) {
    this.router = router;
    this.repository = repository;
  }

  @Override
  public ApiAnswer<RoutingResponse> route(RoutingRequest request) {
    RoutingResponse response = repository.route(request);
    if (response == null) {
      ApiAnswer<RoutingResponse> route = router.route(request);
      if (route.isSuccess()) {
        response = route.response();
        repository.save(request, response);
      } else {
        return ApiAnswer.failure(route.code(), route.message());
      }
    }
    return ApiAnswer.success(response);
  }
}
