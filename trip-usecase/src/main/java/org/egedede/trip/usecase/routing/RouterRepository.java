package org.egedede.trip.usecase.routing;

/** holds data. Should not query another service */
public interface RouterRepository {
  void save(RoutingRequest request, RoutingResponse response);
  RoutingResponse route(RoutingRequest request);
}
