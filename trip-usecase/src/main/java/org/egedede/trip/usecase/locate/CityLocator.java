package org.egedede.trip.usecase.locate;

import java.util.List;

import org.egedede.trip.ApiAnswer;
import org.egedede.trip.exception.TechnicalException;

/**
 * Locates a city
 */
public interface CityLocator {
    ApiAnswer<List<CityLocation>> locate(CityLocatorRequest request) throws TechnicalException;
}
