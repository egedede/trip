package org.egedede.trip.usecase.routing;

import java.time.Duration;

import org.egedede.trip.model.Distance;
import org.egedede.trip.model.Location;
import org.egedede.trip.model.RoutingMode;

public record RoutingResponse(
    RoutingMode routingMode, Location start, Location end, Distance distance, Duration duration) {
}
